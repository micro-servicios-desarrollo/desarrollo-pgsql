<?php

namespace App\Http\Controllers;

use App\Generacionbackup;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class GeneracionBackupController extends Controller
{
    use ApiResponser;

    public function __construct(){
        //
    }

    public function index(){
        $Backup = DB::table('v_generacionbackup')
        ->paginate(ENV('PAGINATE'));
        return response()->json($Backup);
    }

    public function indexyear($id, Request $request){
        $Backup = DB::table('v_generacionbackup')
        ->where('year', '=', $id)
        ->paginate(ENV('PAGINATE'));
        return response()->json($Backup);
    }

    public function indexmonth($id, $idm, Request $request){
        $Backup = DB::table('v_generacionbackup')
        ->where('year', '=', $id)
        ->where('month', '=', $idm)
        ->paginate(ENV('PAGINATE'));
        return response()->json($Backup); 
    }

    public function detail(){
        $Backup = DB::table('v_generacionbackupdetalle')
        ->paginate(ENV('PAGINATE'));
        return response()->json($Backup); 
    }

    public function detailyear($id, Request $request){
        $Backup = DB::table('v_generacionbackupdetalle')
        ->where('year', '=', $id)
        ->paginate(ENV('PAGINATE'));
        return response()->json($Backup); 
    }

    public function detailmonth($id, $idm, Request $request){
        $Backup = DB::table('v_generacionbackupdetalle')
        ->where('year', '=', $id)
        ->where('month', '=', $idm)
        ->paginate(ENV('PAGINATE'));
        return response()->json($Backup); 
    }
}
