<?php

$router->get('/backuppgsql', 'GeneracionBackupController@index');
$router->get('/backuppgsql/{id}', 'GeneracionBackupController@indexyear');
$router->get('/backuppgsql/{id}/month/{idm}', 'GeneracionBackupController@indexmonth');
$router->get('/backuppgsqldetail/', 'GeneracionBackupController@detail');
$router->get('/backuppgsqldetail/{id}', 'GeneracionBackupController@detailyear');
$router->get('/backuppgsqldetail/{id}/month/{idm}', 'GeneracionBackupController@detailmonth');
